/*
 * Public API Surface of ayoba-angular
 */

export * from './lib/ayoba-angular.service';
export * from './lib/ayoba-angular.module';
