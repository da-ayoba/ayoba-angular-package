import { TestBed } from '@angular/core/testing';

import { AyobaAngularService } from './ayoba-angular.service';

describe('AyobaAngularService', () => {
  let service: AyobaAngularService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AyobaAngularService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
