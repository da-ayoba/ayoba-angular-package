import { Injectable } from '@angular/core';

export function getNativeWindow(): any {
  return window;
}
@Injectable({
  providedIn: 'root'
})
export class AyobaAngularService {

  private _ayoba: any;
  private ready : any;     //created the ready variable

  constructor() {
    this._ayoba = this._getAyoba();
  }

  private _getAyoba(): any {
    const userAgent = navigator.userAgent || navigator.vendor || getNativeWindow()["opera"];

    if (/windows phone/i.test(userAgent)) {
      return null;
    }

    if (/android/i.test(userAgent)) {
      return getNativeWindow()["Android"];
    }

    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      return null;
    }

    return "unknown";
  }

  hasAyoba(): boolean {
    return this._ayoba && this._ayoba != "unknown" ? true : false;
  }

//created the start function
  start(){
    //Now that presence is updated and Ayoba is initialised, let's try calling a few functions
    this.ready = true;
    console.log("Let's try calling available methods..")
    if (Object.getOwnPropertyNames(this._ayoba).includes("getSelfJid")) {
        console.log("Calling getSelfJid()...");
    };

    if (Object.getOwnPropertyNames(this._ayoba).includes("getMsisdn")) {
        console.log("Calling getMsisdn()...");
    };

    if (Object.getOwnPropertyNames(this._ayoba).includes("getCountry")) {
    };

    if (Object.getOwnPropertyNames(this._ayoba).includes("getLanguage")) {
    };
}

//start function ends here

  finish(): void {
    this._ayoba.finish();
  }

  composeMessage(message: string): void {
    this._ayoba.composeMessage(message);
  }

  sendMessage(message: string): void {
    this._ayoba.sendMessage(message);
  }

  sendLocation(lat: string, lon: string): void {
    this._ayoba.sendLocation(lat, lon);
  }

  onLocationSentResponse(responseCode: any) {
    getNativeWindow()["responseCode"] = responseCode;
  }

  sendMedia(url: string, mime: string): void {
    this._ayoba.sendMedia(url, mime);
  }

  onMediaSentResponse(responseCode: any, encodedUrl: any) {
    getNativeWindow()["responseCode"] = responseCode;
    getNativeWindow()["encodedUrl"] = encodedUrl;
  }

  getMsisdn(): string {
    return this._ayoba.getMsisdn();
  }

  getCountry(): string {
    return this._ayoba.getCountry();
  }

  getLanguage(): string {
    return this._ayoba.getLanguage();
  }

  canSendMessage(): boolean {
    return this._ayoba.canSendMessage();
  }

  onLocationChanged(lat: string, lon: string): void {
    getNativeWindow()["currentLatitude"] = lat;
    getNativeWindow()["currentLongitude"] = lon;
  }

  onProfileChanged(nickname: string, avatarPath: string) {
    getNativeWindow()["currentNickname"] = nickname; 
    getNativeWindow()["currentAvatarPath"] = avatarPath;
  }


  onNicknameChanged(nickname: string) {
    getNativeWindow()["currentNickname"] = nickname;
    if (!this.ready){ this.start();}; //added this line of code 
  }

  onAvatarChanged(avatarPath: any) {
    getNativeWindow()["currentAvatarPath"] = avatarPath;
  }

  onPresenceChanged(presence: any) {
    getNativeWindow()["currentPresence"] = presence;
  }

  takePicture(): any {
    return this._ayoba.takePicture();
  }

  onPictureRetrievedResponse(responseCode: any, picturePath: any) {
    getNativeWindow()["responseCode"] = responseCode;
    getNativeWindow()["picturePath"] = picturePath;
  }

  getContacts() {
    getNativeWindow()["jsonContacts"] = this._ayoba.getContacts();
  }

  startConversation(jid: string) {
    return this._ayoba.startConversation(jid);
  }

}
