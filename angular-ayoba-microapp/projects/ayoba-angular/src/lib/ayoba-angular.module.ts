import { InjectionToken, NgModule } from '@angular/core';
import { AyobaAngularService } from './ayoba-angular.service';

@NgModule({
  declarations: [
  ],
  imports: [
  ],
  exports: [
  ],
  providers: [
    AyobaAngularService,
  ]
})
export class AyobaAngularModule { }
