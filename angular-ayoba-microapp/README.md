# Ayoba Angular Package

Ayoba Microapp API provides a list of operations that can be used by external developers to retrieve some user allowed information and run some actions over [Ayoba](https://ayoba.me/). This module provides the functionality required for an Angular micro app to make use of the operations provided by the API and effectively integrate with Ayoba. 

# Files

- ayoba-angular.module.ts
- ayoba-angular.service.ts

## Ayoba-angular.module.ts

Project root module.

## Ayoba-angular.service.ts

Determines the mobile operating system and returns/ interacts with the correct interface. The service also wraps Ayoba functionalities in easy to use functions as shown in the example projects at https://gitlab.com/ayoba_hackathon/microapp.

### Functions found on the service
 - _getAyoba(): determines mobile operating system and returns correct interface
 - hasAyoba(): does a check to see that an interface is returned
 #### API related functionality
 Go to https://gitlab.com/ayoba_hackathon/microapp/-/wikis/home to find out more about the Ayoba Microapp APIs functionality found within the service.

## Installation

Install the package:
<pre><code>npm install @ayoba/angular</code></pre>
